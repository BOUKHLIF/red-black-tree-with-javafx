package trees;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import trees.gui.BasePane;
import trees.gui.RBPane;
import trees.implementation.RedBlackTree;

import java.util.ArrayList;

public class Main extends Application {
    private static ArrayList<Integer> nodes = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        BorderPane pane = new BorderPane();
        RBPane view = new RBPane(tree);
        setPane(pane, view, tree);
        setStage(pane, primaryStage, "RedBlackTree Visualisation");
    }

    private void setStage(BorderPane pane, Stage primaryStage, String title) {
        Scene scene = new Scene(pane, 500, 500);
        primaryStage.setTitle(title);
        primaryStage.getIcons().add(new Image("file:data/tree.png"));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setPane(BorderPane pane, BasePane view, RedBlackTree<Integer> tree) {
        pane.setCenter(view);
        TextField textField = new TextField();
        textField.setPrefColumnCount(3);
        textField.setAlignment(Pos.BASELINE_RIGHT);
        Button insert = new Button("Insert");
        Button delete = new Button("Delete");
        Button search = new Button("Search");
        handlers(textField, insert, search, delete, tree, view);
        HBox hBox = new HBox(5);
        hBox.getChildren().addAll(new Label("Enter a value"), textField, insert, search, delete);
        hBox.setAlignment(Pos.BASELINE_CENTER);
        pane.setBottom(hBox);
    }

    private void handlers(TextField textField, Button insert, Button search, Button delete, RedBlackTree<Integer> tree, BasePane view) {
        insert.setOnAction(e -> {
            if (textField.getText().length() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "You haven't entered anything!", ButtonType.OK);
                alert.getDialogPane().setMinHeight(80);
                alert.show();
            } else {
                int key = Integer.parseInt(textField.getText());
                nodes.add(key);
                if (tree.search(key)) {
                    view.displayTree();
                    view.setStatus(key + " is already present!");
                } else {
                    tree.insert(key);
                    view.displayTree();
                    view.setStatus(key + " is inserted!");
                }
                textField.clear();
            }
        });

        search.setOnAction(e -> {
            if (textField.getText().length() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "You haven't entered anything!", ButtonType.OK);
                alert.getDialogPane().setMinHeight(80);
                alert.show();
            } else {
                int key = Integer.parseInt(textField.getText());
                view.displayTree();
                if (tree.search(key)) {
                    view.setStatus(key + " exists");
                } else {
                    view.setStatus(key + " does not exist");
                }
                textField.clear();
            }
        });

        delete.setOnAction(e -> {
            int key = Integer.parseInt(textField.getText());
            if (!tree.search(key)) {
                view.displayTree();
                view.setStatus(key + " is not present!");
            } else {
                tree.delete(key);
                view.displayTree();
                view.setStatus(key + " is replaced by its predecessor and is deleted!");
            }
            textField.clear();
        });
    }
}
